package com.example.bruno.footballscoreboard

import android.app.Dialog
import android.graphics.Typeface
import android.os.Bundle
import android.os.PersistableBundle
import android.os.SystemClock
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_scoreboard.*
import kotlinx.android.synthetic.main.dialog_extra_time.*

class ScoreboardActivity : AppCompatActivity() {

    val TIME_PERIOD: Long = (45*60*1000)
    var totalTime: Long = TIME_PERIOD
    var isRunning = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scoreboard)

        changeEnabledButtons(false)

        val counterCustomFont = Typeface.createFromAsset(getAssets(), "fonts/KARNIVOD.ttf")
        chStopwatch.typeface = counterCustomFont
        tvPeriod.typeface = counterCustomFont
        tvExtraTime.typeface = counterCustomFont
        tvTeamAScore.typeface = counterCustomFont
        tvTeamBScore.typeface = counterCustomFont
        tvTeamAYellowCard.typeface = counterCustomFont
        tvTeamBYellowCard.typeface = counterCustomFont
        tvTeamARedCard.typeface = counterCustomFont
        tvTeamBRedCard.typeface = counterCustomFont

        val teamNameCustomFont = Typeface.createFromAsset(getAssets(), "fonts/KARNIVOF.ttf")
        tvTeamAName.typeface = teamNameCustomFont
        tvTeamBName.typeface = teamNameCustomFont
        tvTeamAName2?.typeface = teamNameCustomFont
        tvTeamBName2?.typeface = teamNameCustomFont


        btnPlay.setOnClickListener{
            //iniciar o timer
//            chStopwatch.base = SystemClock.elapsedRealtime() - (44 * 60 * 1000) - 30000
            chStopwatch.base = SystemClock.elapsedRealtime()
            chStopwatch.start()
            isRunning = true

            //habilita os botões
            changeEnabledButtons(true)
        }

        btnReset.setOnClickListener{
            chStopwatch.stop()
            chStopwatch.base = SystemClock.elapsedRealtime()
            isRunning = false

            //desabilita botões
            changeEnabledButtons(false)

            setDefaultValues()
        }

        btnAddTime.setOnClickListener{
            //dialog para adicionar tempo extra
            val dlgExtraTime = Dialog(this)
            dlgExtraTime.setContentView(R.layout.dialog_extra_time)
            dlgExtraTime.npTimeExtra.maxValue = 15
            dlgExtraTime.npTimeExtra.minValue = 0

            dlgExtraTime.btnOk.setOnClickListener{
                dlgExtraTime.dismiss()
                val extraTime = dlgExtraTime.npTimeExtra.value
                tvExtraTime.text = "+${extraTime}"
                totalTime += extraTime*60*1000
            }

            dlgExtraTime.btnCancel.setOnClickListener{
                dlgExtraTime.dismiss()
            }

            dlgExtraTime.show()

        }

        btnGoalForTeamA.setOnClickListener{
            tvTeamAScore.increase()
        }

        btnGoalForTeamB.setOnClickListener{
            tvTeamBScore.increase()
        }

        btnAddYellowCardTeamA.setOnClickListener{
            tvTeamAYellowCard.increase()
        }

        btnAddYellowCardTeamB.setOnClickListener{
            tvTeamBYellowCard.increase()
        }

        btnAddRedCardTeamA.setOnClickListener{
            tvTeamARedCard.increase()
        }

        btnAddRedCardTeamB.setOnClickListener{
            tvTeamBRedCard.increase()
        }

        chStopwatch.setOnChronometerTickListener {

            Log.d("tempo", "Total: ${totalTime} e Percorrido:${SystemClock.elapsedRealtime() - it.base}")


            if((SystemClock.elapsedRealtime() - it.base) >= totalTime){
                if(tvPeriod.text.equals("1º")) {
                    tvPeriod.text = "2º"
                    tvExtraTime.text = ""
                    chStopwatch.stop()
                    chStopwatch.base = SystemClock.elapsedRealtime()
                    changeEnabledButtons(false)
                    Toast.makeText(this, getString(R.string.start_2st), Toast.LENGTH_SHORT).show()
                    isRunning = false
                } else{
                    chStopwatch.stop()
                    chStopwatch.base = SystemClock.elapsedRealtime()
                    changeEnabledButtons(false)
                    Toast.makeText(this, getString(R.string.end_game), Toast.LENGTH_SHORT).show()
                    changeEnabledButtons(false)
                    btnPlay.isEnabled = false
                    btnReset.isEnabled = true
                    isRunning = false
                }
            }

        }

    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.run {
            if(isRunning) putLong("stopwatch", chStopwatch.base)
            putCharSequence("period", tvPeriod.text)
            putCharSequence("extraTime", tvExtraTime.text)
            putCharSequence("teamAScore", tvTeamAScore.text)
            putCharSequence("teamBScore", tvTeamBScore.text)
            putCharSequence("teamAYellowCard", tvTeamAYellowCard.text)
            putCharSequence("teamBYellowCard", tvTeamBYellowCard.text)
            putCharSequence("teamARedCard", tvTeamARedCard.text)
            putCharSequence("teamBRedCard", tvTeamBRedCard.text)
            putBoolean("isRunning", isRunning)
        }

        super.onSaveInstanceState(outState)

    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        savedInstanceState?.run {
            isRunning = getBoolean("isRunning")
            if(isRunning) chStopwatch.base = getLong("stopwatch")
            tvPeriod.text = getCharSequence("period")
            tvExtraTime.text = getCharSequence("extraTime")
            tvTeamAScore.text = getCharSequence("teamAScore")
            tvTeamBScore.text = getCharSequence("teamBScore")
            tvTeamAYellowCard.text = getCharSequence("teamAYellowCard")
            tvTeamBYellowCard.text = getCharSequence("teamBYellowCard")
            tvTeamARedCard.text = getCharSequence("teamARedCard")
            tvTeamBRedCard.text = getCharSequence("teamBRedCard")
        }

        if(isRunning) {
            changeEnabledButtons(true)
            chStopwatch.start()
        }

        super.onRestoreInstanceState(savedInstanceState)
    }

    fun setDefaultValues() {
        //zera os gols e cartões
        tvPeriod.text = "1º"
        tvExtraTime.text = ""
        totalTime = TIME_PERIOD
        tvTeamAScore.text = "0"
        tvTeamBScore.text = "0"
        tvTeamAYellowCard.text = "0"
        tvTeamBYellowCard.text = "0"
        tvTeamARedCard.text = "0"
        tvTeamBRedCard.text = "0"
        btnPlay.isEnabled = true
    }

    fun changeEnabledButtons(state: Boolean) {
        btnReset.isEnabled = state
        btnAddTime.isEnabled = state
        btnGoalForTeamA.isEnabled = state
        btnGoalForTeamB.isEnabled = state
        btnAddYellowCardTeamA.isEnabled = state
        btnAddYellowCardTeamB.isEnabled = state
        btnAddRedCardTeamA.isEnabled = state
        btnAddRedCardTeamB.isEnabled = state
    }

    fun TextView.increase(): Unit {
        var number: Int = Integer.parseInt(text as String)
        number++
        text = number.toString()
    }

}
